################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../src/aeabi_romdiv_patch.s 

C_SRCS += \
../src/clkconfig.c \
../src/cr_startup_lpc11uxx.c \
../src/crp.c \
../src/gpio.c \
../src/main.c \
../src/nmi.c \
../src/system_LPC11Uxx.c \
../src/timer16.c \
../src/timer32.c \
../src/uart.c 

OBJS += \
./src/aeabi_romdiv_patch.o \
./src/clkconfig.o \
./src/cr_startup_lpc11uxx.o \
./src/crp.o \
./src/gpio.o \
./src/main.o \
./src/nmi.o \
./src/system_LPC11Uxx.o \
./src/timer16.o \
./src/timer32.o \
./src/uart.o 

C_DEPS += \
./src/clkconfig.d \
./src/cr_startup_lpc11uxx.d \
./src/crp.d \
./src/gpio.d \
./src/main.d \
./src/nmi.d \
./src/system_LPC11Uxx.d \
./src/timer16.d \
./src/timer32.d \
./src/uart.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.s
	@echo 'Building file: $<'
	@echo 'Invoking: MCU Assembler'
	arm-none-eabi-gcc -c -x assembler-with-cpp -DDEBUG -D__CODE_RED -DCORE_M0 -D__LPC11UXX__ -D__REDLIB__ -g3 -mcpu=cortex-m0 -mthumb -specs=redlib.specs -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -DDEBUG -D__CODE_RED -DCORE_M0 -D__LPC11UXX__ -D__REDLIB__ -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m0 -mthumb -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


