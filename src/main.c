//*****************************************************************************
//   +--+
//   | ++----+
//   +-++    |
//     |     |
//   +-+--+  |
//   | +--+--+
//   +----+    Copyright (c) 2011 Code Red Technologies Ltd.
//
// LED flashing SysTick application for LPCXPresso11U14 board
//
// Software License Agreement
//
// The software is owned by Code Red Technologies and/or its suppliers, and is
// protected under applicable copyright laws.  All rights are reserved.  Any
// use in violation of the foregoing restrictions may subject the user to criminal
// sanctions under applicable laws, as well as to civil liability for the breach
// of the terms and conditions of this license.
//
// THIS SOFTWARE IS PROVIDED "AS IS".  NO WARRANTIES, WHETHER EXPRESS, IMPLIED
// OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE APPLY TO THIS SOFTWARE.
// USE OF THIS SOFTWARE FOR COMMERCIAL DEVELOPMENT AND/OR EDUCATION IS SUBJECT
// TO A CURRENT END USER LICENSE AGREEMENT (COMMERCIAL OR EDUCATIONAL) WITH
// CODE RED TECHNOLOGIES LTD.
//
//*****************************************************************************

#ifdef __USE_CMSIS
#include "LPC11Uxx.h"
#endif


#include "LPC11Uxx.h"

#include "gpio.h"

#include <time.h>

#define ON 0
#define OFF 1

#define IN 0
#define OUT 1

void delay (int n){
	for(int i=0; i<n;i+=2)
	{
	//	printf("delay~~~");
	}

}

void setPins(){
 	     //0 intrare, 1 iesire
	GPIOSetDir(0, 9, OUT); //LED este de iesire
	GPIOSetDir(0, 1, OUT); //LED este de iesire
	GPIOSetDir(0, 2, OUT); //LED este de iesire
	GPIOSetDir(0, 3, OUT); //LED este de iesire
	GPIOSetDir(0, 4, OUT); //LED este de iesire
	GPIOSetDir(0, 5, OUT); //LED este de iesire
	GPIOSetDir(0, 7, OUT); //LED este de iesire

    //0 intrare, 1 iesire
	GPIOSetDir(0, 21, IN); // Btn este de intrare
	GPIOSetDir(1, 23, IN); // Btn este de intrare
	GPIOSetDir(0, 11, IN); // Btn este de intrare
}

void displayNumber(int number){
	switch(number){
		case 1:
			GPIOSetBitValue(0,9, OFF);
			GPIOSetBitValue(0,1, ON);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, OFF);
			GPIOSetBitValue(0,4, OFF);
			GPIOSetBitValue(0,5, OFF);
			GPIOSetBitValue(0,7, OFF);
			break;
		case 2:
			GPIOSetBitValue(0,9, ON);
			GPIOSetBitValue(0,1, ON);
			GPIOSetBitValue(0,2, OFF);
			GPIOSetBitValue(0,3, ON);
			GPIOSetBitValue(0,4, ON);
			GPIOSetBitValue(0,5, OFF);
			GPIOSetBitValue(0,7, ON);
			break;
		case 3:
			GPIOSetBitValue(0,9, ON);
			GPIOSetBitValue(0,1, ON);
			GPIOSetBitValue(0,2, ON);
			GPIOSetBitValue(0,3, ON);
			GPIOSetBitValue(0,4, OFF);
			GPIOSetBitValue(0,5, OFF);
			GPIOSetBitValue(0,7, ON);
			break;
		default:
			GPIOSetBitValue(0,9, OFF);
			GPIOSetBitValue(0,1, OFF);
			GPIOSetBitValue(0,2, OFF);
			GPIOSetBitValue(0,3, OFF);
			GPIOSetBitValue(0,4, OFF);
			GPIOSetBitValue(0,5, OFF);
			GPIOSetBitValue(0,7, OFF);
			break;
	}
}

void getInput(uint32_t* desiredFloor){
	  if (!GPIOGetPinValue(0,21))
	  {
		  *desiredFloor=1;
	  }else if(!GPIOGetPinValue(1,23)){
		  *desiredFloor=2;
	  }else if(!GPIOGetPinValue(0,11)){
		  *desiredFloor=3;
	  }
}

void getNextFloor(uint32_t* currentFloor, uint32_t* desiredFloor){
	if(*desiredFloor){
		delay(500000);
		if(*currentFloor < *desiredFloor){
			*currentFloor+=1;
		}else if(*currentFloor == *desiredFloor){
			*desiredFloor = 0;
		}
		else if(*currentFloor > *desiredFloor && *desiredFloor !=0){
			*currentFloor -=1;
		}
	}
}

int main(void) {
	  GPIOInit();
	  setPins();

	  uint32_t currentFloor = 1;
	  uint32_t desiredFloor = 0;

	  while (1){
		  getInput(&desiredFloor);
	  	  displayNumber(currentFloor);
	  	  getNextFloor(&currentFloor, &desiredFloor);
	  } //end while
}

